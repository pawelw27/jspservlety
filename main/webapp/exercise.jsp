<%@page import="java.util.*" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix = "fmt"  uri = "http://java.sun.com/jsp/jstl/fmt"  %>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
      integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
<body>
<h2>Hello World!</h2>

<p> HELLO! the time is now  <%= new java.util.Date() %>
</p> <%-- moze byc bez <p></p> --%>

<p>Dzisiaj jest <% out.println(java.time.LocalDate.now().toString());%></p>

<%
    System.out.println("Evaluating date now");
    Date date = new Date();
%>

<br> <%-- dodatkowa linijka --%>

Hello! The time is now <%=date%>

<table border="2">

    <%for (int i = 0; i < 10; i++) {%>
    <tr>
        <td>Number</td>
        <td><%=i + 1%>
        </td>
    </tr>
    <% } %>

</table>

<% boolean hello = false;%>
<% if (hello) {%>

<p> Hello, world</p>

<% } else { %>

<p>Goodbye, world</p>

<% } %>

Going to include hello.jsp... <br>
<%@ include file="hello.jsp" %>

<%! private long visitCount = 0; %>

<h2>Ilosc odwiedzin strony: <%= ++visitCount%>
</h2>

<%! Date theDate = new Date();

    Date getDate() {
        System.out.println("In getDate() method");
        return theDate;
    }%>

Hello! The time from method is now <%=getDate()%>

<%-- to widze tylko tu --%>
<!-- to widze na stronie -->

Going to include hello.jsp...<br>
<jsp:include page="hello.jsp"/>

<c:set var="salary" value="2000"/>
<c:out value="${salary}" />
<c:if test="${salary > 1}">
    <p>content</p>
</c:if>

<c:set var="number" value="500"/>
<c:choose>
    <c:when test="${number>300 or number<400}">
        <p>Hurra!</p>
    </c:when>
    <c:when test="${number<300}">
        <p>Nooooo!</p>
    </c:when>
    <c:when test="${number>1}">
        <p>Toooooo!</p>
    </c:when>

</c:choose>

<c:set var="alphabet"
       value="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']}"
       scope="application" />

<c:forEach var="index" begin = "0" end = "10">
    <c:out value="${index}"></c:out>
</c:forEach>
<br>
<c:forEach var="index" begin = "0" end = "10">
    <c:out value="${alphabet[index]}"></c:out>
</c:forEach>
<br>
<c:forEach items="alphabet" >
    <c:out value="${alphabet}"></c:out>
</c:forEach>
<br>
${fn:length(alphabet)}
<br>
${fn:contains(alphabet, 'A')}
<br>
${fn:endsWith(alphabet, z)}
<br>

<c:set var="formatNumber" value="46588.77"/>
<c:out value="${formatNumber}"/>
<fmt:formatNumber value="${formatNumber}" type="currency"/>

<h2>Parametry szukania</h2>
<p>
    Szukane slowo : <b><%=request.getParameter("query")%></b><br/>
    Strona: <b><%=request.getParameter("page")%></b><br/>
    Sortowanie: <b><%=("desc".equals(request.getParameter("sort"))? "malejaco" : "rosnaco")%></b>
    <br>

    <c:out value="${param.get('query')}"/>
    <c:out value="${param.get('page')}"/>
    <c:choose>
    <c:when test="${param.sort eq 'desc'}">malejaco</c:when>
    <c:when test="${param.sort ne 'desc'}">rosnaco</c:when>
    </c:choose>

<form action="search.jsp" method=POST>
    <div>
        Slowo: <input type="text" name="query"/>
    </div>
    Strona: <input type="text" name="page"/><br/>
    </div>
    Sortowanie:
    <select name="sort">
        <option value="asc">rosnaco</option>
        <option value="desc">malejaco</option>
    </select><br/>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

</p>
</body>
</html>
