
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LOGOUT</title>
</head>
<body>

<%
    session.invalidate();
%>
<form action="session.jsp" method="GET">

    <button type="submit">Go Back</button>

</form>

</body>
</html>
