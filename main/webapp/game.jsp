<%@page import="java.util.*" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix = "fmt"  uri = "http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MEGA Guessing Game</title>
</head>
<body>
<h1>MEGA Guessing Game</h1>

<p>By opening this site random number from 1 to 100 was generated...</p>
<p>Try to guess it!</p>
<%! private int randNumber; %>
<%! private Random random = new Random(); %>

<%
    if(("GET").equals(request.getMethod())){
        randNumber = random.nextInt(100) + 1;
    }
%>

<c:set var="randomNumber" value="<%=randNumber%>"/>

<form action="game.jsp" method="POST">

    <div>
        Write your guess number here: <input type="number" name="guessNumber">
    </div>

    <div>
       and press the button: <button type="submit">GO!</button>
    </div>

</form>
<c:choose>
    <c:when test="${param.guessNumber gt randomNumber}">Your number is too high, TRY AGAIN!</c:when>
    <c:when test="${param.guessNumber lt randomNumber}">Your number is too low, TRY AGAIN!</c:when>
    <c:when test="${param.guessNumber eq randomNumber}">

        <h3>Congratulations - YOU'RE THE BOSS!<h3>
            <br>
            play again by clicking link below, SO MUCH FUN!
            <br>
            <a href="<c:url value="game.jsp"/> ">ANOTHER GO</a>

    </c:when>
</c:choose>

<%
    Cookie cookie = new Cookie("searchID", String.valueOf(4546));
    cookie.setMaxAge(60*60*24);
    response.addCookie(cookie);
%>

</body>
</html>
