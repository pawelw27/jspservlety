<%@page import="java.util.*" %>
<%@page import="javax.servlet.http.Cookie" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cookie</title>
</head>
<body>

<h1>Cookie</h1>


<% Cookie cookie = new Cookie("VisitCounter", String.valueOf(0));
    cookie.setMaxAge(60 * 60 * 24);

    Cookie[] cookies = request.getCookies();
    int count = 0;
    for (Cookie cookieFromTable : cookies) {
        if (cookieFromTable.getName().equals("VisitCounter")) {
            count = Integer.parseInt(cookieFromTable.getValue());
            count++;
            cookie.setValue(count + "");
        }
    }
    response.addCookie(cookie);
%>

<p>licznik odwiedzin strony: <%=cookie.getValue()%>
</p>


</body>
</html>
