<%@page import="java.util.*" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix = "fmt"  uri = "http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>

<h1>LOGIN - fussion of jsp and servlet</h1>

<c:if test="${not empty wrong}">
    <div class="alert alert-danger">
        <c:out value="${wrong}"/>
    </div>
</c:if>

<form action="/app/FirstDispatcher" method="post">
    <div>
        <p>Login:</p>
        <input type="text" name="login">
    </div>
    <div>
        <p>Password :</p>
        <input type="password" name="password">
    </div>
    <button type="submit">SEND</button>
</form>


</body>
</html>
