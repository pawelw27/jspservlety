<%@page import="java.util.*" %>
<%@page import="javax.servlet.http.Cookie" %>
<%@ page import="java.time.LocalDate" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>

<%

    HttpSession sessionCounter = request.getSession();
    String visitCountKey = new String("visitCount");


        Integer visitCount = (Integer) sessionCounter.getAttribute(visitCountKey);
        if(visitCount == null) {
            visitCount = 0;
        }

        visitCount = new Integer(visitCount.intValue() + 1);
        sessionCounter.setAttribute(visitCountKey, visitCount);

%>

Liczba odwiedzin: <%=sessionCounter.getAttribute("visitCount")%><br>
creation time: <%=new Date(sessionCounter.getCreationTime())%><br>
timestamp of last user request : <%=new Date(sessionCounter.getLastAccessedTime())%><br>
max time of sessionCounter: <%=sessionCounter.getMaxInactiveInterval()/60%> minutes<br>
show ID: <%=sessionCounter.getId()%><br>
is new?: <%=sessionCounter.isNew()%><br>

<form action="logout.jsp" method="POST">

    <button type="submit">Invalidate</button>

</form>





</body>
</html>
