<%@page import="java.util.*" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix = "fmt"  uri = "http://java.sun.com/jsp/jstl/fmt"  %>
<html>
<body>

<h1>CHOOSE YOUR PATH</h1>

<a href="exercise.jsp">EXE - everything and nothing</a> <br>
<a href="cookie.jsp">COOKIE - visit counter by cookie design</a> <br>
<a href="game.jsp">GAME!</a> <br>
<a href="session.jsp">SESSION - visit counter and data</a> <br>
<a href="chat.jsp">CHAT - talking with myself</a> <br>
<a href="/app/hello">SERVLET the first</a> <br>
<a href="/app/company">COMPANY exercise</a> <br>
<a href="/app/cookie">COOKIE exercise</a> <br>
<a href="/app/GPPD">GetPostPutDelete exercise</a> <br>

<form action="/app/GPPD" method="post">
    <h2>Enter GPPD - send parameters</h2>
    <div>
        <p>Parametr 1:</p>
        <input type="text" name="param1" id="param1">
    </div>
    <div>
        <p>Parametr 2:</p>
        <input type="text" name="param2" id="param2">
    </div>
    <button type="submit">SEND</button>
</form>

<a href="login.jsp">LOGIN - fussion of servlet and jsp</a> <br>
<a href="registry.jsp">REGISTER - another fussion of servlet and jsp</a> <br>

</body>
</html>
