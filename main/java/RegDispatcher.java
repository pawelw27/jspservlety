import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "RegDispatcher", value = "/RegDispatcher")
public class RegDispatcher extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = req.getRequestDispatcher("registry.jsp");

        String password = req.getParameter("password");
        String repPassword = req.getParameter("repeatPassword");
        String login = req.getParameter("login");

        PrintWriter out = resp.getWriter();

        if (password != repPassword) {

            req.setAttribute("passwordNotTheSame", "passwords are not identical - fix it");
            dispatcher.forward(req, resp);
        }else{
            out.println("welcome new User :"+login);
        }

        //to be continued

    }
}
