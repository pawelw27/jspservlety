import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "firstDispatcher", value = "/FirstDispatcher")
public class FirstDispatcher extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();
        String login = req.getParameter("login");

        if ("twoseven".equals(req.getParameter("password"))) {

            out.println("WELCOME " + login + " You logged in successful");

        } else {

            String wrongPass = "TRY AGAIN - wrong password";
            req.setAttribute("wrong", wrongPass);

            RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");
            dispatcher.forward(req, resp);
        }

    }
}
