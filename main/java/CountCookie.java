import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Parameter;

@WebServlet(name = "Cookie", value = "/cookie")
public class CountCookie extends HttpServlet {

//to samo na session w GPPD
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cookie cookie = new Cookie("VisitCounter", String.valueOf(0));
        cookie.setMaxAge(60 * 60 * 24);

        Cookie[] cookies = request.getCookies();
        int count = 0;
        for (Cookie cookieFromTable : cookies) {
            if (cookieFromTable.getName().equals("VisitCounter")) {
                count = Integer.parseInt(cookieFromTable.getValue());
                count++;
                cookie.setValue(count + "");
            }
        }
        response.addCookie(cookie);

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();
        out.println("cześć jesteś tu : "+cookie.getValue()+ " osobą");

    }

}
