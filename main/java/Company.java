import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Parameter;

@WebServlet(name = "Company", value = "/company")
public class Company extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean youCanSeeFooter = false;
        if("yes".equals(request.getParameter("footer"))){
            youCanSeeFooter = true;
        }

        String year = request.getParameter("year");
        String company = request.getParameter("company");

        PrintWriter out = response.getWriter();
        out.println(year+"<br>");
        out.println(company+"<br>");

        if(youCanSeeFooter) {
            response.setContentType("text/html;charset=UTF-8");
            out.println("(" + year + ") <a href='http://samsung.com'>" + company + "</a>"); //stopka - footer
        }
    }

}
